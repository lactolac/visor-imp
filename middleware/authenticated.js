export default function ({ route, store, redirect }) {
  const loginPath = '/auth/login'
  if (!store.state.authenticated) {
    if (route.path != loginPath) {
      return redirect(loginPath)
    }
  }
}