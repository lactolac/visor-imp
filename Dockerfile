FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY ./dist ./
ENV HOST 0.0.0.0