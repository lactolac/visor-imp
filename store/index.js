export const state = () => ({
  authenticated: false,
  username: '',
  USER_ID: null
})

export const mutations = {
  logged(state, payload) {
    const { status, username, USER_ID } = payload
    state.authenticated = status
    state.username = username
    state.USER_ID = USER_ID
  }
}
